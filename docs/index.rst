.. changelogdir documentation master file, created by
   sphinx-quickstart on Sat Jun  3 19:03:19 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to changelogdir's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   INSTALLATION
   USAGE
   CHANGELOG
