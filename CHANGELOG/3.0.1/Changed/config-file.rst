- changelogdir now uses the first of the following files that it finds in the
  current directory as config file:  ``.changelogdirrc``, ``changelogdirrc`` or
  ``setup.cfg``.
