Changelog
=========

Unreleased - YYYY-MM-DD
----------------------------------------------------------------------------

This section contains all entries that are not yet released.  When it's release
time, simply move the entries that are being released to the section of the
corresponding release.

This entry appears before all other entries in this section.

You can write any valid
`reStructuredText <http://docutils.sourceforge.net/rst.html>`_ or
`Markdown <http://commonmark.org/>`_ or
`whatever <https://en.wikipedia.org/wiki/List_of_markup_languages>`_ here.
(Though, admittedly, changelogdir is best suited for Markdown-like languages.)

- A typical entry might look like this, with one bullet point per thing that has
  changed.

- One entry can of course contain multiple bullet points.

  * And you can nest bullet points if you want.

If you want, you can add a footer beneath all your entries in a section.
Perhaps you can list and thank your contributors here?  Whatever you like.

2.0.0 - 
----------------------------------------------------------------------------

1.0.0 - The year of the Linux desktop
----------------------------------------------------------------------------

If you like the format suggested by
`Keep a Changelog <http://keepachangelog.com>`_, you can put all your entries in
subsections, as demonstrated in this section.

Security
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Some obscure security fix.

- (The 000_Security subsection appears before all other subsections.)

Added
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Made a really good feature.

  To use this feature, do::

    sudo rm -fr /  # Don't do this.

Deprecated
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- X11 has been deprecated in favour of Wayland. (One may dream.)
