This section contains all entries that are not yet released.  When it's release
time, simply move the entries that are being released to the section of the
corresponding release.

This entry appears before all other entries in this section.

You can write any valid
`reStructuredText <http://docutils.sourceforge.net/rst.html>`_ or
`Markdown <http://commonmark.org/>`_ or
`whatever <https://en.wikipedia.org/wiki/List_of_markup_languages>`_ here.
(Though, admittedly, changelogdir is best suited for Markdown-like languages.)
