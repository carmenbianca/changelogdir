- A typical entry might look like this, with one bullet point per thing that has
  changed.

- One entry can of course contain multiple bullet points.

  * And you can nest bullet points if you want.
