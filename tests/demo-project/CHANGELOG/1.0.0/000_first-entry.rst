If you like the format suggested by
`Keep a Changelog <http://keepachangelog.com>`_, you can put all your entries in
subsections, as demonstrated in this section.
